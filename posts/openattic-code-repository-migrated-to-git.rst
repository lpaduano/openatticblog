.. title: openATTIC code repository migrated to git
.. slug: openattic-code-repository-migrated-to-git
.. date: 2017-04-05 15:16:20 UTC+02:00
.. tags: contributing, development, git, mercurial, opensource
.. category:
.. link:
.. description: Announcing the move of our upstream code repo to git
.. type: text
.. author: Lenz Grimmer

From the very beginning, the `openATTIC source code repository
<https://bitbucket.org/openattic/openattic/>`_ was managed using the `Mercurial
<https://www.mercurial-scm.org/>`_ distributed source control management tool.

Our code is hosted on `BitBucket <https://bitbucket.org>`_ which provides for a
tight integration with our `public Jira issue tracker
<https://tracker.openattic.org/>`_.

Unfortunately, Mercurial is somewhat less flexible than git when it comes to
using branches to separate ongoing development work (which is a workflow
encouraged by using Jira/BitBucket) - there is a tight relationship between
branch names across repositories and it's impossible to delete branches once
they have been created or merged. Sure, we could have probably used `Mercurial
bookmarks <https://www.mercurial-scm.org/wiki/Bookmarks>`_ for this, but they
are not well supported by the Jira and BitBucket workflows we are using (and are
more designed to be used locally, not across multiple repositories).

In addition to that, we have a growing developer/contributor base that simply is
more familiar with git than Mercurial nowadays. Switching to git could
potentially help us attracting a bigger number of developers.

These were the main reasons for our decision to convert. Thankfully, with the
help of the `fast-export utility <https://github.com/frej/fast-export>`_, the
actual conversion was straightforward and painless. It automatically renamed the
former ``default`` branch to ``master``, to be conforming to established git
practices. We will revisit the branch naming conventions in a following step.

There was some cleanup work involved afterwards, to remove all the obsolete
branches that were created by Mercurial when merging pull requests.

Additionally, all pending `pull requests
<https://bitbucket.org/openattic/openattic/pull-requests/>`_ from the previous
Mercurial repo had to be re-created, but that provided us with a good
opportunity to clean up the commit history before submitting them. Going
forward, we intend to also make use of the more advanced features of git like
rebasing or squashing change sets, to have a cleaner revision history with less
noise.

We're now in the process of updating our `documentation
<http://docs.openattic.org/>`_ and build scripts to support this change, but
this should not take us too long to resolve.