.. title: openATTIC 2.0.14 beta has been released
.. slug: openattic-2014-beta
.. date: 2016-09-12 16:55:54 UTC+02:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the 2.0.14 beta release
.. type: text
.. author: Lenz Grimmer

Despite the summer holidays, the openATTIC development team has been busy,
adding new functionality and improving existing features. This release also
includes code contributions created by developers not employed by it-novum,
and we're very grateful for the support!

Noteworthy new features include a first implementation of a Ceph Cluster
monitoring dashboard that supports displaying health and performance data of
multiple Ceph clusters.

The openATTIC WebUI now supports multiple dashboards with custom widget
configurations (e.g. title, position and size). The dashboard configuration is
saved in the user's profile and will be restored upon the next login.

See the screenshots below for a preview:

.. slides::

  /galleries/oA-dashboard-2016-09/dashboard01.png
  /galleries/oA-dashboard-2016-09/dashboard02.png

This release also adds extended Ceph pool management support: in addition to
viewing existing pools, it's now possible to also create and delete Ceph pools
via the WebUI, including support for both replicated and erasure-coded pools.

.. TEASER_END

On the "traditional" storage management side, it is now possible to sort the
list of existing volumes by additional criteria like "Used", "Status", "Type"
and "Created".

This release also contains the first version of our task queue implementation
for handling long-running requests, e.g. monitoring the creation of Ceph
placement groups after creating a Ceph pool.

We now also support setting up development environments based on `Vagrant
<https://www.vagrantup.com/>`_. Have a look at our `developer documentation
<http://docs.openattic.org/2.0/developer_docs/vagrant.html>`_ for instructions.

The WebUI test suite also received a number of improvements, for example an
option to create screen shots of the UI in case of test failures. For
development and testing purposes, it's now possible to predefine a user name and
password in the WebUI configuration, to allow a quick login into the user
interface.

To download and install openATTIC, please follow the `installation instructions
<http://docs.openattic.org/2.0/install_guides/index.html>`_ in the
documentation.

**Note:** if you are working on a development branch, it might be necessary to
update the Bower components of the WebUI using the following commands::

  $ cd <oa repo dir>/webui
  $ rm -r app/bower_components/
  $ bower install --allow-root
  $ grunt build

Please note that 2.0.14 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

We would like to thank everyone who contributed to this release, especially
**Brandon Keep** and **Tahaa Karim**! Special thanks go also out to the folks
from SUSE for their continued feedback and support.

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The OP
codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog 2.0.14:

* Backend: Removed unused module 'Clustering' (:issue:`OP-1251`)
* Backend: Fixed rtslib deprecation warning in LIO backend (:issue:`OP-953`)
* Backend: Added a task queue module to track long-running commands
  (:issue:`OP-1360`)
* Backend: Properly handle optional attributes of Ceph pools (:issue:`OP-1416`)
* Backend: Fixed broken call to ``ceph/<fsid>/osds`` on `demo.openattic.org
  <http://demo.openattic.org>`_ (:issue:`OP-1453`)
* Monitoring: Fixed compatibility of the Ceph pool plugin with Django 1.6
  (:issue:`OP-1393`)
* WebUI: Implemented a widget which displays the current state of the Ceph
  cluster (:issue:`OP-85`)
* WebUI: Create and delete ceph pools (:issue:`OP-1299`)
* WebUI: If quicklogin is defined in config.js, the username and password will
  be predefined (:issue:`OP-1386`)
* WebUI: Allow sorting storage volumes by additional fields **Used**,
  **Status**, **Type** and **Created**. Thanks to Tahaa Karim.
  (:issue:`OP-1427`)
* WebUI: Replaced malhar-angular-dashboard with a gridster based implemenation
  (:issue:`OP-1485`)
* WebUI/QA: Extended E2E test configuration by ceph pools and suite path filter
  (:issue:`OP-1339`)
* WebUI/QA: Added screenshot and error log functionality (:issue:`OP-1375`)
* WebUI/QA: Have more precised error messages in volume_add site in case of an
  error (:issue:`OP-1376`)
* WebUI/QA: Create and delete ceph pool tests (:issue:`OP-1417`)
* WebUI/QA: Fix a timing problem and create needed RBDs (:issue:`OP-1461`)
* WebUI/QA: Fix the timing problems with in the RBD creation test
  (:issue:`OP-1471`)
* WebUI/QA: Make e2e outDir and cephCluster attribute optional
  (:issue:`OP-1474`)
* Installation: The ``openattic-gui`` RPM and DEB packages now take care of
  installing an ``index.html`` file in the Apache document root directory that
  performs a redirect to the WebUI. Previously, this task was performed by
  ``oaconfig install``. Thanks to Brandon Keep. (:issue:`OP-959`)
* Development: Add Vagrantfile to easily set up a development environment
  (:issue:`OP-1423`)
* Documentation: Added instructions on how to set up a development system with
  Vagrant (:issue:`OP-1439`)
* Packaging: ``make_dist.py`` doesn't delete anything in the home directory anymore
  (:issue:`OP-1371`)
* Packaging: Simplified the usage of ``make_dist.py`` and prevented
  ``build_deb_packages.py`` from hanging indefinitely (:issue:`OP-1431`)
* Packaging: Fixed a bug where folder names had to be ``openattic`` to be
  correctly packaged using ``make_dist.py`` (:issue:`OP-1447` and
  :issue:`OP-1449`)
* Packaging: Added a script ``utils/build_rpm.sh`` to the repository that converts
  a source tar archive created by ``make_dist.py`` into installable RPM packages
  (using `rpm/openattic.spec`) (:issue:`OP-1117`)
