.. title: openATTIC 3.5.3 has been released
.. slug: openattic-353-has-been-released
.. date: 2017-10-30 12:01:09 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.5.3 release
.. type: text
.. author: Sebastian Wagner

We are happy to announce version 3.5.3 of openATTIC.

3.5.3 is a small bugfix release mainly containing fixes for upgrade bugs from openATTIC 2.0.

This release also features fixes regarding deletion of RGW users with existing buckets
and the health widget in the classic dashboard widget.

.. TEASER_END

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.


Changelog for version 3.5.3
---------------------------

Changed
~~~~~~~
* WebUI: Now it is possible to delete a RGW user with buckets (:issue:`OP-2790`)
* WebUI/QA: Update Karma config with Cobertura report and ChromeHeadless
  (:issue:`OP-2725`)
* Backend: Use health's "status" instead of "overall_status" (:issue:`OP-2845`)

Fixed
~~~~~
* Backend: Fixed db corruption when running `oaconfig install` (:issue:`OP-2784`)
* Backend: Fixed Settings page doesn't save new keyring information properly
  (:issue:`OP-2729`)
* Backend: Fixed "No SystemD in ``ifconfig.systemapi``" Message (:issue:`OP-2774`)
* Backend: The Grafana dashboard will now be shown automatically when
  upgrading from oA 2.x to 3.x (:issue:`OP-2843`)
* Deployment: Add ``PreReq: %fillup_prereq`` to SUSE spec file (:issue:`OP-2808`)

