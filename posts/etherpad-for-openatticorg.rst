.. title: Etherpad for openATTIC.org
.. slug: etherpad-for-openatticorg
.. date: 2016-06-28 17:12:47 UTC+02:00
.. tags: etherpad  
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

We now have our own etherpad hosted on `etherpad.openattic.org <https://etherpad.openattic.org>`_.

Enjoy your collaborative editing in really real-time.
