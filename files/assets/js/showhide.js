function toggle() {
	var $spoilerLink = $('#spoilerLink'),
		$spoilerBody = $('#spoilerBody'),
		defaultShowText = 'show',
		defaultHideText = 'hide';

	if ($spoilerLink.data('show-text') != '') {
		defaultShowText = $spoilerLink.data('show-text');
	}

	if ($spoilerLink.data('hide-text') != '') {
		defaultHideText = $spoilerLink.data('hide-text');
	}

	if ($spoilerBody.css('display') == 'block') {
		$spoilerBody.css('display', 'none');
		$spoilerLink.html(defaultShowText);
	} else {
		$spoilerBody.css('display', 'block');
		$spoilerLink.html(defaultHideText);
	}
} 

$(document).ready(function(){
	var $spoilerLink = $('#spoilerLink'),
		$spoilerBody = $('#spoilerBody'),
		defaultShowText = 'show',
		defaultHideText = 'hide';

	$spoilerLink
		.click(function(){
			toggle();
		})
		.css('cursor', 'pointer');

	if ($spoilerLink.data('show-text') != '') {
		defaultShowText = $spoilerLink.data('show-text');
	}

	if ($spoilerLink.data('hide-text') != '') {
		defaultHideText = $spoilerLink.data('hide-text');
	}

	if ($spoilerBody.css('display') == 'block') {
		$spoilerLink.html(defaultHideText);
	} else {
		$spoilerLink.html(defaultShowText);
	}
});

