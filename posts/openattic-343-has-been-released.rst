.. title: openATTIC 3.4.3 has been released
.. slug: openattic-343-has-been-released
.. date: 2017-08-17 18:59:50 UTC+02:00
.. tags: announcement, ceph, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 3.4.3 release
.. type: text
.. author: Stephan Müller

We are happy to announce version 3.4.3 of openATTIC.

With the new feature development for openATTIC 3.x slowing down, this release is primarily focused on fixing bugs (e.g. an issue leading to error messages when deleting RBDs) and improving details in the existing functionality.

New features include the possibility to create RBDs with a dedicated data pool and to enable compression on Ceph pools stored on Bluestore OSDs. You can now change the password of a user via the openATTIC WebUI. We also added some new statistics for RBDs and Ceph Object Gateway (based on Grafana & Prometheus). We also updated the documentation and improved the usabillity of the UI in several areas. We have also worked on speeding up the loading of the list of RBDs by loading the data in parallel. In addition to that, this version also concludes the removal of support for Nagios/Icinga and PNP4Nagios.

.. TEASER_END

This version includes a known regression bug - the WebUI currently does not work properly in environments without DeepSea (see OP-2571 for details). A fix for this is already under review and will be included in the next version.

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.4.3
----------------------------

Added
~~~~~
* WebUI: Enabled grafana statistics for RBDs (:issue:`OP-2475`)
* WebUI: Enable providing a dedicated data pool when creating RBDs (:issue:`OP-2315`)
* WebUI: Add Ceph Object Gateway user statistics (:issue:`OP-2214`)
* Development: Add support for ECMAScript6 using Babel (:issue:`OP-1890`)
* Documentation: Add documentation how to use local settings (:issue:`OP-2549`)
* WebUI: Add Pool compression (:issue:`OP-2470`)
* Backend: Add "/api/host/current" service to get the current openATTIC host (:issue:`OP-2496`)
* WebUI: Display openATTIC version on the settings page (:issue:`OP-2496`)
* Packaging: `make_dist.py` now replaces the Version: tag in the RPM spec file automatically (:issue:`OP-2256`)
* WebUI: Add helper text for NFS "pseudo" and "tag" (:issue:`OP-2554`)

Changed
~~~~~~~
* Development: Upgrade Vagrant box to openSUSE Leap 42.3 (:issue:`OP-2579`)
* Development: Ensure that the webserver and oA systemd are killed when the Vagrant helper scripts are interrupted via CTRL-C (:issue:`OP-2578`)
* WebUI: Execute requests in parallel to speedup RBD list loading (:issue:`OP-2552`)
* WebUI: Migration of Ceph iSCSI controllers to components (:issue:`OP-2372`)
* WebUI: Removed expert settings from RBD form (:issue:`OP-2518`)
* WebUI: Improved usability of modal dialogs (:issue:`OP-2560`)
* Documentation: Updated Ceph keyring installation chapter (:issue:`OP-2505`)
* Packaging: Renamed openattic RPM spec files (:issue:`OP-1950`)
* WebUI: The password of a user can be changed now (:issue:`OP-4`)
* WebUI: Remove unnecessary toasty messages (:issue:`OP-2563`)
* WebUI: NFS Tag displayed only for NFSv3 protocol (:issue:`OP-2555`)
* Documentation: Documentation cleanups and updates (:issue:`OP-2580`)

Fixed
~~~~~
* WebUI/QA: Unstuck RBD and pool creation tests (:issue:`OP-2574`)
* WebUI/QA: Fixed element not clickable in task-queue suite (:issue:`OP-2262`)
* Backend: Failed to create RBD with expert settings enabled but without any selected features (:issue:`OP-2541`)
* Backend: Handle IPv6 netmask with suffix gracefully (:issue:`OP-2547`)
* Backend: Added user name and keyring file information to RBD command call (:issue:`OP-2553`)
* Backend: Fixed hanging of `systemctl stop openattic-systemd` (:issue:`OP-2444`)
* Backend: Fixed error in `get_rbd_performance_data` task by catching an exception (:issue:`OP-2057`)
* WebUI: Fixed default action when selecting multiple RBDs (:issue:`OP-2546`)
* Installation: fix ownership/permissions of `/var/log/openattic` in the SUSE RPM package to fix a logrotate failure (:issue:`OP-2517`)
* WebUI: Fixed rule set selection problem in pool form (:issue:`OP-2564`)
* WebUI: Javascript error on edit NFSv3 export (:issue:`OP-2576`)

Removed
~~~~~~~
* Backend: The Nagios backend module has been removed due to Prometheus/Grafana integration (:issue:`OP-2436`)

