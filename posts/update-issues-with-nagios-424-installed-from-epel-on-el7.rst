.. title: Update Issues with Nagios 4.2.4 installed from EPEL on EL7
.. slug: update-issues-with-nagios-424-installed-from-epel-on-el7
.. date: 2017-03-10 16:38:26 UTC+01:00
.. tags: bug, centos, nagios, monitoring, rhel, update, workaround
.. category: 
.. link: 
.. description: Summarizing known issues with Nagios 4.2.4 and openATTIC
.. type: text
.. author: Lenz Grimmer

We've observed that the `update of Nagios from version 4.0.8 to 4.2.4 in EPEL7
<https://bugzilla.redhat.com/show_bug.cgi?id=1329857>`_ is causing some issues
for both new openATTIC installations and updates of existing installations on
Red Hat Enterprise Linux and derivatives like CentOS.

In this post, we will summarize the issues that affect openATTIC and how to
resolve them.

.. TEASER_END

When updating Nagios from 4.0.8 to 4.2.4 on a previously installed and
configured openATTIC system, you will first have to fix the Nagios
configuration.

As the ``nagios.cfg`` file has to be modified in order to `enable PNP4Nagios
<http://docs.openattic.org/2.0/install_guides/oA_installation.html#configure-pnp4nagios-on-el7>`_,
RPM will (righfully) refuse to replace it during an update and will place the
new version into ``/etc/nagios/nagios.cfg.rpmnew`` instead. Unfortunately, the
Nagios update also changes a lot of default values like directory names and file
locations, so Nagios 4.2.4 will not start with a configuration file that was
initially shipped with version 4.0.8.

If you haven't done any other changes to this file other than adding the
PNP4Nagios configuration as outlined in the openATTIC installation instructions,
the easiest way to resolve this problem to rename
``/etc/nagios/nagios.cfg.rpmnew`` to ``/etc/nagios/nagios.cfg`` and then
re-apply the changes necessary to enable PNP4Nagios as outlined in the manual.

Moreover, the location of the ``status.dat`` file has changed, which breaks the
``oaconfig install`` command.

For both updating an existing system or a new installation on EL7, you will have
to edit ``/etc/default/openattic`` after the initial installation and change the
path names for the settings ``NAGIOS_STATUS_DAT_PATH`` and ``NAGIOS_STATUS_DAT``
from ``/var/log/nagios/status.dat`` to ``/var/spool/nagios/status.dat``.

We submitted a bug report about this upstream, but at this point it's not
feasible to roll back these changes, to avoid further breakage: `EPEL7: Updating
Nagios from 4.0.8 to 4.2.4 breaks existing installations
<https://bugzilla.redhat.com/show_bug.cgi?id=1427895>`_

We'll update the EL7 configuration file ``/etc/default/openattic.conf`` in the
upcoming openATTIC 2.0.19 release to adapt to this change in the Nagios 4.2.4
package in EPEL7. This is tracked in :issue:`OP-1955` on our public issue
tracker.