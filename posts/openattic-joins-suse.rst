.. title: openATTIC joins SUSE
.. slug: openattic-joins-suse
.. date: 2016-11-08 04:25:47 UTC+01:00
.. tags: anouncement, community, collaboration, opensource, suse
.. category: 
.. link: 
.. description: Commenting on the acquisition of openATTIC by SUSE
.. type: text
.. author: Lenz Grimmer

.. image:: ../../images/SUSE-and-openATTIC.jpg

You may have seen `today's announcement
<https://www.suse.com/de-de/newsroom/post/2016/suse-poised-for-greater-growth-in-software-defined-storage-market-by-acquiring-openattic-storage-management-assets-from-it-novum/>`_,
that the openATTIC development team has joined `SUSE <https://suse.com/>`_, and
with this SUSE has taken over the corporate sponsor role from openATTIC's parent
company, `it-novum <http://it-novum.com>`_.

I'd like to share my view about what this means for openATTIC and the community
and ecosystem around the project.

First off, the license of the software or openness of the development process
won't change. Quite the contrary: SUSE is fully committed to keeping openATTIC
licensed under the GPL and growing the community around the project.

You will still be able to freely use it without arbitrary restrictions for your
Ceph and "traditional" storage management needs.

.. TEASER_END

We had a joint development collaboration around the Ceph management
functionality with SUSE for quite some time already. This transition is taking
the good relationship that we've established with the SUSE engineers to the next
level.

I'm happy to confirm that all the developers that previously worked for it-novum
have been hired by SUSE and will continue to contribute to openATTIC.

In fact, SUSE plans to add more people to the team in the future, to further
accelerate the development and growth of the project.

SUSE will support the upstream openATTIC project in a fashion very similar to
how they sponsor the `openSUSE Linux Distribution <https://opensuse.org/>`_.

As a result of our ongoing collaboration, openATTIC will be part of SUSE's
upcoming Ceph-based `SUSE Enterprise Storage 4
<https://www.suse.com/products/suse-enterprise-storage/>`_ product, and will
provide web-based cluster management and monitoring functionality.

We will continue to freely provide installable packages for the major Linux
distributions that we support already. In the near future, we plan to improve
our release build processes and the package repository provisioning by
migrating our release builds and package repositories from our internal build
systems over to the `openSUSE Build Service <https://build.opensuse.org/>`_
(OBS).

In addition to the various SUSE Linux variants, OBS supports building and
distributing software packages for other Linux distributions (such as
RHEL/CentOS, Debian and Ubuntu) and provides a well-established network of
mirror sites, which will help to make future openATTIC releases more easily
available and installable.

Being able to directy tap into the vast engineering resources that SUSE
provides will help us to further improve openATTIC in terms of functionality,
quality and robustness. In addition to the Ceph management and monitoring
features, we will also continue to expand and improve the more "traditional"
storage management features of openATTIC like NFS, CIFS or iSCSI, to make it a
viable alternative to proprietary storage solutions.

SUSE's reputation and culture as a true Open Source company is a very good fit
for us as a team, and the company's global reach will hopefully help us to
further grow and foster an international community of users and developers
around the project. Being backed by a much larger corporate entity will also
allow users to select openATTIC for their storage needs with confidence.

This development confirms that the changes that we implemented over the course
of the past 1.5 years were heading in the right direction. Fully opening up
our tools and processes and removing the dual licensing helped a lot with
attracting more users and contributors and also played a role in capturing
SUSE's attention.

We are very excited about this change and we look forward to the new
opportunities and possibilities that this transition offers to us and our
users!
