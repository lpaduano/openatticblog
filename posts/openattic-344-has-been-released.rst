.. title: openATTIC 3.4.4 has been released
.. slug: openattic-344-has-been-released
.. date: 2017-09-01 12:21:59 UTC+02:00
.. tags: announcement, ceph, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.4.4 release
.. type: text
.. author: Patrick Nawracay

We are happy to announce version 3.4.4 of openATTIC.

Starting with Ceph "Luminous", all pools need to be associated to the
application using the pool. With this release, we have introduced the
possibility to manage these associations in our frontend and enable you to use
Cephs' own pre-defined applications as well as creating your own ones. The edit
functionality is still in the works, but didn't make it into this release. It
will be included in the next one.

We have also fixed a few bugs related to the communication with DeepSea and
decoupled a few requirements. Hence, the OSDs, RBDs and the Pools tab can now be
used without having to use DeepSea.

Additionally, we have further enhanced the functionality of our WebUI by
allowing to modify existing Ceph Object Gateway user capabilities.

.. TEASER_END

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users`_ Google Group, visit our
`#openattic`_ channel on `irc.freenode.net`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP
codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public JIRA instance`_.

From the Changelog for version 3.4.4:

Added
~~~~~
* WebUI: No clusters configured message on settings page (OP-2535)
* Backend: Provide Ceph pool application metadata in the API (OP-2593)
* Node list is now accessible without DeepSea (OP-2602)
* WebUI: Create pools with application tags (OP-2591)

Changed
~~~~~~~
* WebUI: Relocate cluster related code from oaCephModuleLoader to
  oaCephClusterLoader (OP-2513)
* WebUI: Improve forms initial rendering (OP-2559)
* WebUI: Allow the modification of existing RGW user capabilities
  (OP-2319)
* Backend: Removed some outdated and unused Django models (OP-2586)
* Backend: Prefer the openattic (firstly) and the admin (secondly) keyring user
  over any other (OP-2523)
* Remove on mon_hosts, mon_initial_members and public_address from DeepSea
  pillar (OP-2606)

Fixed
~~~~~
* WebUI: OSD, RBD and Pool tabs are broken without DeepSea (OP-2571)
* WebUI: Error after remove iSCSI "Discovery authentication" (OP-2550)
* Packaging: Fixed regression in make_dist.py which caused `cache push` to fail
  on empty cache (OP-2587)
* WebUI: Fix wrong angular module declaration (OP-2588)
* WebUI: Ceph health widget does not display the error and warning messages
  (OP-2610)
* Backend: Fixed systemd error "Not an absolute path, ignoring: ${SYSD_PIDFILE}"
  in service file (OP-2570)
* WebUI/QA: Refactored pool form element verification (OP-2609)
* Backend: Fixed internal server error, if DeepSea's pillar data doesn't contain
  "public_network" (OP-2595)

Removed
~~~~~~~
* Packaging: Removed all unnecessary (local storage related) packages from the
  spec file (OP-2211)

.. _openATTIC Users: https://groups.google.com/forum/#%21forum/openattic-users
.. _#openattic: irc://irc.freenode.org/#openattic
.. _irc.freenode.net: http://webchat.freenode.net/?channels=openattic
.. _public JIRA instance: http://tracker.openattic.org/
