.. title: IMAP: Subfolders missing in thunderbird
.. slug: imap-subfolders-missing-in-thunderbird
.. date: 2015-11-03 10:15:17 UTC+01:00
.. tags: imap, exchange, thunderbird, email
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

I switched from Outlook to Thunderbird and after i configured my email within Thunderbird my whole subfolders were missing. After a little while i found a checkbox that fixes this problem.

Check: -> "Account Settings" -> "Server Settings" -> "Advanced" -> "Server supports folders that contain subfolders and messages"

Now you have to restart your thunderbird and voila :-).
